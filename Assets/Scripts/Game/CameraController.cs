﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    // Start is called before the first frame update
    public Transform cameraGhost;
    public Transform cameraRig;


    void FixedUpdate()
    {
        Vector3 ghostRotation=cameraGhost.rotation.eulerAngles;
        Vector3 thisRotation= transform.rotation.eulerAngles;
        transform.rotation =Quaternion.Euler(thisRotation.x,ghostRotation.y,thisRotation.z);

        transform.position=new Vector3(cameraRig.position.x, transform.position.y, cameraRig.position.z);

    }
}
