﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;
public class GameController : MonoBehaviour
{
    public static int levelCount = 0;
    public static bool isGameOn = true;
    public static bool isLevelCompleted = false;
    public static bool isTruckDestroyed = false;
    UIController uiController;
    TruckController truck;
    TrackController track;
    public GameObject[] Levels;
    void Awake()
    {
        levelCount = PlayerPrefs.GetInt("levelCount")%Levels.Length;
        Instantiate(Levels[levelCount]);
        truck = GameObject.FindGameObjectWithTag("Truck").GetComponent<TruckController>();
        track = GameObject.FindGameObjectWithTag("Track").GetComponent<TrackController>();
        uiController = GameObject.FindGameObjectWithTag("UIController").GetComponent<UIController>();
        uiController.setLevelTexts(levelCount);
    }
    void Update()
    {
        uiController.setProgress(truck.roadProgress / track.roadLenght);
    }
    public  void restartGame()
    {
        isGameOn = true;
        isLevelCompleted = false;
        isTruckDestroyed = false;
        SceneManager.LoadScene("Game", LoadSceneMode.Single);
    }
    public  void pauseGame()
    {

        isGameOn = false;
    }
    public  void gameover()
    {
        uiController.setActiveGameOverScreen(true);
        isGameOn = false;
        isTruckDestroyed = true;
        stopEnemyCubes();
    }
    public  void stopEnemyCubes()
    {
        GameObject[] enemyCubes = GameObject.FindGameObjectsWithTag("EnemyCube");
        foreach (GameObject enemyCube in enemyCubes)
        {
            try
            {
                Rigidbody enemyRigidBody = enemyCube.GetComponent<Rigidbody>();
                enemyRigidBody.isKinematic = true;
            }
            catch
            {

            }
        }
    }
    public  void levelPassed()
    {
        uiController.setActiveLevelCompletedScreen(true);
        isGameOn = false;
        isLevelCompleted = true;
    }
    public  void playerTouched()
    {
        if (isLevelCompleted)
        {
            loadNextLevel();
        }
        else if (isTruckDestroyed)
        {
            restartGame();
        }
    }
    public  void loadNextLevel()
    {
        levelCount += 1;
        PlayerPrefs.SetInt("levelCount", levelCount);
        restartGame();
    }



}
