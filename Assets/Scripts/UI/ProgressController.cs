﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressController : MonoBehaviour
{

    //Progress UI controller
    public Text currentLevelText;
    public Text nextLevelText;


    public Scrollbar stageScrollBar;
    public GameObject StageHandle;
    public void setProgress(float progress)
    {

            stageScrollBar.size = progress;
            if (!StageHandle.activeSelf && progress>0)
            {
                StageHandle.SetActive(true);
            }
        
    }

    public void setLevelTexts(int currentLevel)
    {
        currentLevelText.text = (currentLevel + 1).ToString();
        nextLevelText.text = (currentLevel + 2).ToString();
    }
}
