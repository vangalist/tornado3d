﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIController : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject gameOverScreen;
    public GameObject levelCompletedScreen;
    public ProgressController progressController;

    public void setActiveGameOverScreen(bool active)
    {
        gameOverScreen.SetActive(active);
    }
    public void setActiveLevelCompletedScreen(bool active)
    {
        levelCompletedScreen.SetActive(active);
    }


    public void setProgress(float progress){
        progressController.setProgress(progress);
    }
    public void setLevelTexts(int currentLevel){
        progressController.setLevelTexts(currentLevel);
    }


}
