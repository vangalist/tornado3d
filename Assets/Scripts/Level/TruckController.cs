﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TruckController : TrackTracker
{

    private TornadoController tornado;
    public float roadProgress = 0;
    private Vector3 currentPosition;
    private Vector3 prevPosition;

    private GameController gameController;

    void Awake()
    {
        roadProgress = 0;
        currentPosition = transform.position;
        prevPosition = currentPosition;

        gameController = GameObject.FindGameObjectWithTag("Game").GetComponent<GameController>();
    }
    void FixedUpdate()
    {
        if (GameController.isGameOn)
        {
            if (GameController.isGameOn)
            {
                if (isTrackEnded)
                {
                    gameController.levelPassed();
                }
                else
                {
                    go2NextTrackPoint();
                    if (isRotating)
                    {
                        rotateTracker();

                    }
                }
            }
            updateRoadProgress();

        }
    }
    void updateRoadProgress()
    {
        currentPosition = transform.position;
        roadProgress += (currentPosition - prevPosition).magnitude;
        prevPosition = currentPosition;
    }
    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Tornado" || col.tag == "EnemyCube")
        {
            gameController.gameover();
            Debug.Log("truck is here");
        }

    }

}
