﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackTracker : MonoBehaviour
{

    private TrackController track;

    public static float TrackerVelocity = 10f;
    public int currentTrackPoint = 0;
    public bool isTrackEnded = false;
    public bool isRotating = false;
    float currentRotation_y;
    public float desiredRotation_y;
    public float rotationSpeed = 5f;

    public float rotationAngularVelocity = 0;
    void Start()
    {
        track = GameObject.FindGameObjectWithTag("Track").GetComponent<TrackController>();

    }

    void FixedUpdate()
    {
        if (GameController.isGameOn)
        {
            if (isTrackEnded)
            {
            }
            else
            {
                go2NextTrackPoint();
                if (isRotating)
                {
                    rotateTracker();

                }
            }
        }
    }
    protected void rotateTracker()
    {

        transform.rotation = Quaternion.Euler(
            transform.rotation.eulerAngles.x,
            transform.rotation.eulerAngles.y + rotationAngularVelocity * Time.deltaTime,
            transform.rotation.eulerAngles.z);

        if (Mathf.Abs((transform.rotation.eulerAngles.y - desiredRotation_y) % 360) < rotationSpeed / 14)
        {
            transform.rotation = Quaternion.Euler(
                transform.rotation.eulerAngles.x,
                desiredRotation_y,
                transform.rotation.eulerAngles.z);
            isRotating = false;
        }
    }
    protected void go2NextTrackPoint()
    {
        gameObject.transform.position += track.nextTrackPointDirection(currentTrackPoint) * TrackerVelocity * Time.deltaTime;
        if (track.isReachedTrackPoint(currentTrackPoint, transform.position, TrackerVelocity / 30))
        {
            Debug.Log("reachedTrackpoint");
            currentTrackPoint += 1;
            if (currentTrackPoint == track.pathObjects.Count - 1)
            {
                isTrackEnded = true;
            }
            else
            {
                transform.position = track.pathObjects[currentTrackPoint].position;
                rotateTracker2TrackDirection();
            }
        }
    }
    void rotateTracker2TrackDirection()
    {
        isRotating = true;
        currentRotation_y = transform.rotation.eulerAngles.y;
        bool isRotationRight = track.angleBetweenRoads(currentTrackPoint) > 0;
        rotationAngularVelocity = isRotationRight ? rotationSpeed : -1 * rotationSpeed;

        desiredRotation_y = currentRotation_y + track.angleBetweenRoads(currentTrackPoint);
    }

}
