﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    private Transform Tornado;
    private Rigidbody my_rb;
    private float forceMultiplier = .5f;
    private float constantForceMultiplier = 0.5f;
    private float maxRange = 100f;
    void Start()
    {
        Tornado = GameObject.FindGameObjectWithTag("Tornado").GetComponent<Transform>();
        my_rb = gameObject.GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        if (GameController.isGameOn)
        {
            Vector3 distanceVector = Tornado.position - transform.position;
            float distance = distanceVector.magnitude;
            if (distance < TornadoController.shellWidth)
            {
                pushAwayFromTornado(distanceVector);
            }
            else if (distance > maxRange)
            {
                Destroy(gameObject);
            }
        }

    }
    void pushAwayFromTornado(Vector3 distanceVector)
    {
        Vector3 calculatedForce = calculateForce(distanceVector);
        my_rb.AddForce(calculatedForce, ForceMode.Impulse);
    }
    private Vector3 calculateForce(Vector3 distanceVector)
    {
        return (constantForceMultiplier + forceMultiplier / distanceVector.magnitude) * distanceVector.normalized * -1;
    }
}
