﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadCreator : MonoBehaviour
{
    private TrackController track;
    public GameObject roadPrefab;
    public GameObject cornerPrefab;
    public GameObject finishLinePrefab;
    public static float roadWidth = 20f;
    void Start()
    {
        track = GameObject.FindGameObjectWithTag("Track").GetComponent<TrackController>();
        generateAllRoads();
    }
    void generateAllRoads()
    {
        generateCorner(track.pathObjects[0]);
        for (int i = 1; i < track.pathObjects.Count-1; i++)
        {
            generateCorner(track.pathObjects[i]);
            generateRoad(track.pathObjects[i - 1].position, track.pathObjects[i].position, i % 2 == 1);
        }
        //generate finishline and road
        int lastTrackPointIndex= track.pathObjects.Count-1;
       Vector3 directionOfRoad = (track.pathObjects[lastTrackPointIndex].position-track.pathObjects[lastTrackPointIndex - 1].position).normalized;
       generateRoad(track.pathObjects[lastTrackPointIndex - 1].position,
                    track.pathObjects[lastTrackPointIndex].position , 
                    lastTrackPointIndex % 2 == 1);
        Vector3 positionOfFinishLine = track.pathObjects[lastTrackPointIndex].position+directionOfRoad*roadWidth/4- 2*Vector3.up;
        GameObject finishline = Instantiate(finishLinePrefab,positionOfFinishLine,Quaternion.identity);
        if(lastTrackPointIndex % 2 == 0){
            finishline.transform.rotation=Quaternion.Euler(0,90,0);
        }



    }
    void generateCorner(Transform cornerTransform)
    {
        Vector3 CornerPosition = new Vector3(cornerTransform.position.x, cornerTransform.position.y - 2, cornerTransform.position.z);
        Instantiate(cornerPrefab, CornerPosition, Quaternion.identity);
    }
    void generateRoad(Vector3 beginPosition, Vector3 endPosition, bool ishorizontal)
    {
        Vector3 center = (beginPosition + endPosition) / 2 - Vector3.up * 2;
        Vector3 distance = beginPosition - endPosition;
        float roadLenght = distance.magnitude;
        GameObject road = Instantiate(roadPrefab, center, Quaternion.identity);
        road.transform.localScale = new Vector3(roadLenght, 2, roadWidth);
        if (ishorizontal)
        {
            road.transform.rotation = Quaternion.Euler(0, 90, 0);
        }

    }

}
