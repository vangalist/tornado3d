﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackController : MonoBehaviour
{
    public Color rayColor = Color.white;
    public List<Transform> pathObjects = new List<Transform>();
    Transform[] theArray;
    public float wireSphereRadious = 0.3f;
    public float roadWidth = 10f;
    public float roadLenght;
    void OnDrawGizmos()
    {
        Gizmos.color = rayColor;
        theArray = GetComponentsInChildren<Transform>();
        pathObjects.Clear();
        foreach (Transform pathObject in theArray)
        {
            if (pathObject != this.transform)
            {
                pathObjects.Add(pathObject);
            }
        }

        for (int i = 0; i < pathObjects.Count; i++)
        {
            Vector3 position = pathObjects[i].position;

            if (i > 0)
            {
                bool ishorizontal = i % 2 == 0;
                if(ishorizontal){
                    pathObjects[i].position = new Vector3(pathObjects[i].position.x,pathObjects[i].position.y,pathObjects[i-1].position.z );
                }else{
                    pathObjects[i].position = new Vector3(pathObjects[i-1].position.x,pathObjects[i].position.y,pathObjects[i].position.z );
                }
                Vector3 previous = pathObjects[i - 1].position;
                Gizmos.DrawLine(previous, position);

                drawWireRoad(previous, position);
            }
            Gizmos.DrawWireSphere(position, wireSphereRadious);
        }
    }
    void drawWireRoad(Vector3 start, Vector3 end)
    {

        Vector3 center = (start + end) / 2-Vector3.up*2;
        Vector3 distance = start - end;
        Vector3 scale = new Vector3(Mathf.Abs(distance.x), 2-roadWidth*2, Mathf.Abs(distance.z)) + Vector3.one * roadWidth*2;
        Gizmos.DrawCube(center, scale);
    }
    public Vector3 roadDirection(int current, int next)
    {
        return pathObjects[next].position - pathObjects[current].position;
    }
    public bool isReachedTrackPoint(int currentTrackpoint, Vector3 position,float ErrorMargin)
    {
        Vector3 distanceVector = position - pathObjects[currentTrackpoint + 1].position;
        if (distanceVector.magnitude < ErrorMargin)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public Vector3 nextTrackPointDirection(int currentTrackPoint)
    {
        Vector3 direction = pathObjects[currentTrackPoint + 1].position - pathObjects[currentTrackPoint].position;
        return direction.normalized;
    }
    public float angleBetweenRoads(int middleTrackPoint)
    {
        Vector3 oldDirection = pathObjects[middleTrackPoint].position - pathObjects[middleTrackPoint - 1].position;
        Vector3 newDirection = pathObjects[middleTrackPoint + 1].position - pathObjects[middleTrackPoint].position;
        return Vector3.SignedAngle(oldDirection, newDirection, Vector3.up);
    }
    void Start(){
        calculateRoadLenght();
    }
    void calculateRoadLenght(){
        
        for (int i = 1; i < pathObjects.Count; i++)
        {
            Vector3 position = pathObjects[i].position;
            Vector3 previousPositon = pathObjects[i-1].position;
            roadLenght +=( previousPositon-position).magnitude;
        }
    }
}
