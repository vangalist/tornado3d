﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TornadoController : MonoBehaviour
{
    public Rigidbody TornadoRB;
    public TruckController truckController;
    private GameController gameController;
    private TrackController track;
    public Transform cameraTransform;
    private Vector3 beginTouch;
    private Vector3 endTouch;
    private Vector3 oldTruckPosition;
    private Vector3 newTruckPosition;
    private float dragCoefficent = 40f;
    public static float shellWidth = 50f;

    void Start()
    {
        gameController= GameObject.FindGameObjectWithTag("Game").GetComponent<GameController>();
        track = GameObject.FindGameObjectWithTag("Track").GetComponent<TrackController>();
        oldTruckPosition = truckController.transform.position;
        oldTruckPosition = newTruckPosition;
        TornadoRB.angularVelocity=Vector3.up*250;

    }
    void FixedUpdate()
    {
        if (GameController.isGameOn)
        {
            followTruck();
            if (Input.GetMouseButtonDown(0))
            {
                //Touch Started
                beginTouch = Input.mousePosition;
            }
            else if (Input.GetMouseButton(0))
            {
                //Touch is dragging
                endTouch = Input.mousePosition;
                Vector3 ofset = endTouch - beginTouch;
                dragTornado(convertScreenOfset2WorldOfset(ofset));
                beginTouch = endTouch;
            }
            else if (Input.GetMouseButtonUp(0))
            {
                // Touch is ended
            }
        }else{
            if (Input.GetMouseButtonDown(0))
            {
                gameController.playerTouched();
            }
        }

    }
    Vector3 convertScreenOfset2WorldOfset(Vector3 screenOfset)
    {
        // to normalize ofsets of touches ofset vector is divided to screen Width so it will work properly on wifferent screen sizes
        Vector3 worldOfset = dragCoefficent * (new Vector3(screenOfset.x, 0, screenOfset.y)) / Screen.width;

        // rotate ofset vector to make it same direction with camera and tornado

        worldOfset = Quaternion.Euler(0, cameraTransform.rotation.eulerAngles.y, 0) * worldOfset;

        return worldOfset;
    }
    void followTruck()
    {
        newTruckPosition = truckController.transform.position;
        Vector3 truckMovement = newTruckPosition - oldTruckPosition;
        dragTornado(truckMovement);
        oldTruckPosition = newTruckPosition;
    }
    void dragTornado(Vector3 drag)
    {
        
        Vector3 desiredPosition = gameObject.transform.position + drag;
        Vector3 upRightVector = truckController.transform.position + Vector3.one * RoadCreator.roadWidth * 1.3f;
        Vector3 downLeftVector = truckController.transform.position - Vector3.one * RoadCreator.roadWidth * 1.3f;
        // to be sure that next position of Tornado is valid
        if (isOnAnyRoad(desiredPosition))
        {
            if (desiredPosition.x > upRightVector.x)
            {
                desiredPosition.x = upRightVector.x;
            }
            if (desiredPosition.z > upRightVector.z)
            {
                desiredPosition.z = upRightVector.z;
            }
            if (desiredPosition.x < downLeftVector.x)
            {
                desiredPosition.x = downLeftVector.x;
            }
            if (desiredPosition.z < downLeftVector.z)
            {
                desiredPosition.z = downLeftVector.z;
            }

            gameObject.transform.position = desiredPosition;
        }


    }
    bool isOnaRoad(int roadIndex, Vector3 position)
    {
        Vector3 beginPoint = track.pathObjects[roadIndex - 1].position;
        Vector3 endPoint = track.pathObjects[roadIndex].position;
        float width = track.roadWidth;

        // to do that first find corners of road
        //each point will generate 4 possible points to be corner point
        //but i will pick leftdown and right up corners so each point will generate 2 point only
        float min_x_cordinate = (beginPoint.x > endPoint.x ? endPoint.x : beginPoint.x) - width;
        float min_z_cordinate = (beginPoint.z > endPoint.z ? endPoint.z : beginPoint.z) - width;
        float max_x_cordinate = (beginPoint.x < endPoint.x ? endPoint.x : beginPoint.x) + width;
        float max_z_cordinate = (beginPoint.z < endPoint.z ? endPoint.z : beginPoint.z) + width;

        if (position.x < min_x_cordinate || position.x > max_x_cordinate || position.z < min_z_cordinate || position.z > max_z_cordinate)
        {
            return false;
        }

        return true;
    }
    bool isOnAnyRoad(Vector3 position)
    {

        for (int i = 1; i < track.pathObjects.Count; i++)
        {
            if (isOnaRoad(i, position))
            {
                return true;
            }
        }
        return false;
    }


}
